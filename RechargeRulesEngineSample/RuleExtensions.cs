﻿using RulesEngine.Models;

namespace RechargeRulesEngineSample;

public static class RuleExtensions
{
    public static void PreProcess(this Rule rule, Rule parentRule = null)
    {
        if (!string.IsNullOrWhiteSpace(parentRule?.Expression))
        {
            if (!string.IsNullOrWhiteSpace(rule.Expression))
            {
                rule.Expression = $"({parentRule.Expression}) && ({rule.Expression})";
            }
            else
            {
                rule.Expression = parentRule.Expression;
            }
        }

        if (rule.Rules != null)
        {
            foreach (var childRule in rule.Rules.ToList())
            {
                PreProcess(childRule, rule);
            }
        }
    }
}
