﻿using Newtonsoft.Json;
using RechargeRulesEngineSample;
using RulesEngine.Models;
using System.Text;

var rechargingRulesJson = File.ReadAllText("RechargingRules.json", Encoding.UTF8);

var workflow = JsonConvert.DeserializeObject<Workflow>(rechargingRulesJson);
foreach (var rule in workflow.Rules.ToList())
{
    rule.PreProcess();
}

var rulesEngine = new RulesEngine.RulesEngine(new Workflow[] { workflow });

var ruleResults = await rulesEngine.ExecuteAllRulesAsync("RechargingRules", new RuleParameter("r", new
{
    Money = 300,
    Date = 10.05,
    IsNewCustomer = false,
}));

var outputResults = ruleResults.First().GetOutputResults();

Console.Write("共返");

if (outputResults.TryGetValue("现金", out List<object> moneyList))
{
    var money = moneyList.Select(m => double.Parse(m.ToString())).Max();
    Console.Write($"  {money}元现金");
}

if (outputResults.TryGetValue("积分", out List<object> scoreList))
{
    var score = scoreList.Select(m => double.Parse(m.ToString())).Sum();
    Console.Write($"  {score}积分");
}

Console.ReadLine();